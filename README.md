# Installazione Datapusher su CKAN in ambiente CentOS 7

1. Installazione di *docker-ce*

sudo yum install docker-ce
sudo systemctl start docker
sudo docker --version

2. Scompattare la cartella docker_datapusher 

3. Aprire la cartella ed avviare il build dell'immagine tramite

./build_local.sh 

## NOTE
- se problemi col PROXY durante l'esecuzione:
export http_proxy='<proxy>:<port>'

- per rendere eseguibile il file: 
chmod +x build_local.sh

4. Avviare datapusher

docker start datapusher 

## NOTE
- Datapusher gira sulla porta 8800, se non disponibile si può andare a toccare Dockerfile 
(EXPOSE 8800)

- Se l'installazione è avvenuta correttamente ora su localhost:8800 viene visualizzato il messaggio 
{help: " Get help at: http://ckan-service-provider.readthedocs.org/."}

# NOTE
- sul Dockerfile è possibile specificare il path del datapusher se diverso da quello di default
ENV DATAPUSHER_HOME /usr/lib/ckan/default/datapusher 


5. Configurazione CKAN

- Aggiungere al file di configurazione /etc/ckan/default/development.ini

ckan.datapusher.url = http://0.0.0.0:8800/

ckan.plugins = <other plugins> datapusher

- Riavviare CKAN